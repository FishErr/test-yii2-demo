<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ads".
 *
 * @property int $id
 * @property int $ad_id
 * @property string $model_memo
 * @property string $model_status
 */
class Ads extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ads';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ad_id'], 'integer'],
            [['model_status'], 'string'],
            [['model_memo'], 'string', 'max' => 100],
        ];
    }

    const STATUS_ACTIVE = 'active';
    const STATUS_DELETED = 'deleted';

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ad_id' => 'Ad ID',
            'model_memo' => 'Memo',
            'model_status' => 'Status',
        ];
    }
}
