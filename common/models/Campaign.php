<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 2019-01-24
 * Time: 19:40
 */

namespace common\models;


//use yii\data\ArrayDataProvider;
use yii\data\BaseDataProvider;
use Yii;

class Campaign extends BaseDataProvider
{

    protected $accounts;
    public $accountId;

    public function init()
    {
        parent::init();

        $vk = Yii::$app->authClientCollection->getClient('vkontakte');

        $this->accounts = $vk->post('ads.getCampaigns', ['account_id'=>$this->accountId])['response'];

    }

    protected function prepareModels()
    {
        $models = [];
        foreach ($this->accounts as $account) {
            $models[$account['id']] = $account;
        }
        return $models;
    }
    protected function prepareKeys($models)
    {
        return array_column($this->accounts, 'id');
    }

    protected function prepareTotalCount()
    {
        return count($this->accounts);
    }

}