<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 2019-01-24
 * Time: 19:40
 */

namespace common\models;


//use yii\data\ArrayDataProvider;
use yii\data\BaseDataProvider;
use Yii;

class AdsAPI extends BaseDataProvider
{

    protected $ads;
    public $accountId;
    public $campaignId;

    public function init()
    {
        parent::init();

        $vk = Yii::$app->authClientCollection->getClient('vkontakte');

        $this->ads = $vk->post('ads.getAds', ['account_id' => $this->accountId, 'campaign_ids' => json_encode([$this->campaignId])])['response'];

    }

    protected function prepareModels()
    {
        $models = [];

        foreach ($this->ads as $ad){
            $adModel = Ads::findOne(['ad_id'=>$ad['id']]);
            if(empty($adModel)) {
                $adModel = new Ads([
                    'ad_id'=>$ad['id'],
                    'model_status'=>Ads::STATUS_ACTIVE
                ]);
                $adModel->save();
            }
            if($adModel->model_status == Ads::STATUS_ACTIVE) {
                $models[] = array_merge($ad , ['model'=> $adModel]);
            }
        }
        return $models;
    }

    protected function prepareKeys($models)
    {
        return array_column($this->ads, 'id');
    }

    protected function prepareTotalCount()
    {
        return count($this->ads);
    }

}