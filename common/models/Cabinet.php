<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 2019-01-24
 * Time: 19:40
 */

namespace common\models;


//use yii\data\ArrayDataProvider;
use yii\data\BaseDataProvider;
use Yii;

class Cabinet extends BaseDataProvider
{

    protected $accounts;

    public function init()
    {
        parent::init();

        $vk = Yii::$app->authClientCollection->getClient('vkontakte');

        $this->accounts = $vk->post('ads.getAccounts')['response'];
    }

    protected function prepareModels()
    {
        return $this->accounts;
    }
    protected function prepareKeys($models)
    {
        return array_column($this->accounts, 'account_id');
    }

    protected function prepareTotalCount()
    {
        return count( $this->accounts);
    }

}