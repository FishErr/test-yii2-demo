<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

?>
<div class="site-index">


    <div class="body-content">

        <div class="row">

            <?
            echo yii\grid\GridView::widget([
                'dataProvider' => $campaigns,
                'columns' => [
                    'type',
                    [
                        'label' => 'name',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::a(
                                $data['name'],
                                ['campaigns/list-ads', 'campaignId' => $data['id'], 'accountId' => $this->context->actionParams['accountId']]
                            );
                        }
                    ],
                    'status',
                    'day_limit',
                    'all_limit',
                    'start_time',
                    'stop_time',
                    'create_time',
                    'update_time',
                ],
            ]);

            ?>

        </div>

    </div>
</div>
