<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
?>
<div class="site-index">



    <div class="body-content">

        <div class="row">

        <?
//        var_dump($response);
        echo yii\grid\GridView::widget([
        'dataProvider' => $cabinets,
        'columns' => [
            'account_type',
            'account_status',
            [
                'label' => 'Account Name',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(
                        $data['account_name'],
                        ['campaigns/list-campaigns', 'accountId' => $data['account_id']]
                    );
                }
            ],
            'access_role',
        ],
    ]);

        ?>

        </div>

    </div>
</div>
