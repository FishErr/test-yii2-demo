<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= $model['name'] ?></h3>
    </div>
    <div class="panel-body">

        <?php echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'campaign_id',
                'status',
                'approved',

                'day_limit',
                'all_limit',

                'stop_time',
                'start_time',
                'update_time',
                'ad_format',
                'cpm',
                'impressions_limit',
                'cost_type',
                'weekly_schedule_use_holidays',
                'age_restriction',
                'category1_id',
                'category2_id',
            ],
        ]);
        ?>

        <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

        <?= $form->field($model['model'], 'model_memo', [
            'template' => '{label}{input}{hint}{error}'
        ])->textArea(['rows' => 6, 'enableLabel' => true]); ?>

        <?= $form->field($model['model'], 'id')->hiddenInput()->label(false); ?>

        <div class="form-group">
            <?= Html::submitButton('Save Memo', ['class' => 'btn btn-default']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
    <div class="panel-footer">
        <?= Html::a('Delete Ad', ['delete', 'id' => $model['model']['id']], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
</div>


