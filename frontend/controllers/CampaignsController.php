<?php

namespace frontend\controllers;

use common\models\Ads;
use common\models\AdsAPI;
use common\models\Campaign;
use common\models\Cabinet;
use Yii;
use yii\base\Exception;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * Campaigns controller
 */
class CampaignsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'listCampaigns', 'listAds', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'listCampaigns', 'listAds', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {


        try {

            $cabinets = new Cabinet();

        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }


        return $this->render('index', ['cabinets' => $cabinets]);
    }


    public function actionListCampaigns($accountId)
    {
        \Yii::$app->getView()->params['breadcrumbs'] = ['Кампании'];

        try {
            $campaigns = new Campaign([
                'accountId' => $accountId,
            ]);
        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }


        return $this->render('list-campaigns', ['campaigns' => $campaigns, 'accountId' => $accountId]);

    }


    public function actionListAds($campaignId, $accountId)
    {

        \Yii::$app->getView()->params['breadcrumbs'] = [
            ['label'=>'Кампании', 'url'=> ['list-campaigns', 'accountId'=>$accountId]]
            , 'Обьявления'
        ];

        try {
            $ads = new AdsAPI([
                'campaignId' => $campaignId,
                'accountId' => $accountId
            ]);

            if (Yii::$app->request->isPost) {

                $post = Yii::$app->request->post('Ads');

                $model = Ads::findOne($post['id']);
                $model->load(Yii::$app->request->post());
                if ($model->validate() && $model->save()) {
                    Yii::$app->session->setFlash('success', 'Memo saved');
                } else {
                    foreach ($model->getErrors() as $key => $value) {
                        Yii::$app->session->setFlash('error', $key . ': ' . $value[0]);
                    }
                }
            }

        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->render('list-ads', ['ads' => $ads]);
    }

    public function actionDelete($id)
    {
        $model = Ads::findOne($id);
        $model->model_status = Ads::STATUS_DELETED;
        $model->save();
        return $this->redirect(Yii::$app->request->referrer);

    }

}
